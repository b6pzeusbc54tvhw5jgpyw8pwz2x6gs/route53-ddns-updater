# route53-ddns-updater
This project is based on [route-53-dyndns](https://github.com/JacobSanford/route-53-dyndns).
It is Route53 dynamic dns updater support docker-compose.

## Usage
1. Set your environments
2. Run `docker-compose up -d`
```
$ export AWS_ACCESS_KEY_ID="AKXXXXXXXXXXXXXXXX5A"
$ export AWS_SECRET_ACCESS_KEY="jvXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXFv"
$ export AWS_CONNECTION_REGION="ap-northeast-2"
$ export ROUTE53_HOSTED_ZONE_ID="Z3XXXXXXXXXXWA"
$ export ROUTE53_RECORD_SET_NAME_LIST="*.aluc.io,aluc.io"
$ export ROUTE53_UPDATE_FREQUENCY=6000
$ export IP_GET_URL="http://ipinfo.io/ip"
$ docker-compose up -d
```
`.envrc.example` file may helpful to set environments

## References
- https://github.com/JacobSanford/docker-route53-dyndns
- https://github.com/JacobSanford/route-53-dyndns
