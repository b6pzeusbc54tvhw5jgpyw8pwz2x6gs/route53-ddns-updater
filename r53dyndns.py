#! /usr/bin/env python
"""Updates a Route53 hosted A alias record with the current ip of the system.
"""
from underscore import _
import inspect
import boto.route53
import logging
import os
from optparse import OptionParser
import re
from re import search
import socket
import sys
from urllib2 import urlopen

__author__ = "Jacob Sanford"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Jacob Sanford"
__email__ = "jacob.josh.sanford@gmail.com"
__status__ = "Development"

parser = OptionParser()
parser.add_option('-U', '--url', type='string', dest='ip_get_url', help='URL that returns the current IP address. or it can be set IP_GET_URL. default: http://ipinfo.io/ip')
parser.add_option('-v', '--verbose', dest='verbose', default=False, help='Enable Verbose Output.', action='store_true')
(options, args) = parser.parse_args()

hosted_zone_id = os.getenv('ROUTE53_HOSTED_ZONE_ID')
if hosted_zone_id is None:
    logging.error('Set ROUTE53_HOSTED_ZONE_ID to update')
    sys.exit(-1)

record_set_name_list_to_update = os.getenv('ROUTE53_RECORD_SET_NAME_LIST').split(',')
if len( record_set_name_list_to_update ) < 1:
    logging.warn('Please set ROUTE53_RECORD_SET_NAME_LIST to update ')
    parser.print_help()
    sys.exit(-1)

ip_get_url = options.ip_get_url or os.getenv('IP_GET_URL') or 'http://ipinfo.io/ip'
if ip_get_url is None:
    logging.error('Please specify a URL that returns the current IP address with the -U switch.')
    parser.print_help()
    sys.exit(-1)

if options.verbose:
    logging.basicConfig(
        level=logging.INFO,
    )

content = urlopen(ip_get_url).read().strip()
current_ip = urlopen( ip_get_url ).read().strip().decode('utf-8')
if not bool( re.match(r'[0-9]+(?:\.[0-9]+){3}', current_ip)):
    logging.error("Unable to find an IP address from within the URL:  %s" % ip_get_url)
    sys.exit(-1)

logging.info('found current_ip: ' + current_ip)

try:
    socket.inet_aton(current_ip)
    conn = boto.route53.connect_to_region(os.getenv('AWS_CONNECTION_REGION', 'us-east-1'))
    zone_info = conn.get_hosted_zone(hosted_zone_id)
    zone = conn.get_zone(zone_info.Name)

    def filterRecordSetName(item):
        record_string_info = str(item)
        matched = re.match( r'<Record:(.*?)\.?:A:', record_string_info )
        if matched is None: return None
        name = matched.groups()[0].replace(r'\052', '*')
        return { "name": u''+name, "record": item }

    record_list = list(map(filterRecordSetName, zone.get_records()))
    record_list = _.compact( record_list )

    # print(record_set_name_list_to_update)
    # print(record_list)

    for commonName in record_set_name_list_to_update:

        # info { name: xx, record: <Record ...> }
        info = _.findWhere( record_list, { "name": commonName })
        if not info is None:
            # logging.info( '[' + info['record'].to_print() + ']')
            if current_ip == info['record'].to_print():
                logging.info(commonName + ': Record IP matches, doing nothing')
            else:
                logging.info(commonName + ': IP does not match, update needed')
                zone.delete_a(commonName)
                zone.add_a(commonName, current_ip)
        else:
            logging.info(commonName + ': Record not found, add needed')
            zone.add_a(commonName, current_ip)

except socket.error:
    logging.info('Invalid IP format obtained from URL (' + current_ip + ')')
