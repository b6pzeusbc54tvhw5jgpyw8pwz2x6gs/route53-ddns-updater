#!/bin/sh

if [ -z $ROUTE53_UPDATE_FREQUENCY ]
then
  echo "env ROUTE53_UPDATE_FREQUENCY required"
  exit 1
fi

while :
do
  python /project/r53dyndns.py -v
  echo "Sleeping for $ROUTE53_UPDATE_FREQUENCY seconds..."
  sleep $ROUTE53_UPDATE_FREQUENCY
done
